local down  = 0
local up    = 1
local north = 2
local south = 3
local west  = 4
local east  = 5

local sourceSide    = east
local trash         = west
local enchantedSide = down
local storage       = up

local ts = component.proxy(component.list("transposer")())

local itemMap = {
  ["minecraft:book"]           = storage,
  ["minecraft:bone"]           = storage,
  ["minecraft:dye"]            = storage,
  ["minecraft:enchanted_book"] = storage,
  ["minecraft:fish"]           = storage,
  ["minecraft:leather"]        = storage,
  ["minecraft:name_tag"]       = storage,
  ["minecraft:rotten_flesh"]   = storage,
  ["minecraft:saddle"]         = storage,
  ["minecraft:stick"]          = storage,
  ["minecraft:string"]         = storage,
  ["minecraft:waterlily"]      = storage,
  ["minecraft:bow"]            = trash,
  ["minecraft:bowl"]           = trash,
  ["minecraft:fishing_rod"]    = trash,
  ["minecraft:potion"]         = trash,
  ["minecraft:leather_boots"]  = trash,
  ["minecraft:tripwire_hook"]  = trash
}

if ts == nil then
  error("This program requires a transposer")
end

local function sleep(timeout)
  checkArg(1, timeout, "number", "nil")
  local deadline = computer.uptime() + (timeout or 0)
  repeat
    computer.pullSignal(deadline - computer.uptime())
  until computer.uptime() >= deadline
end

run = true
while run do
  local slots = ts.getAllStacks(sourceSide)
  if slots == nil then
    error("No inventory found on source side (" .. sourceSide ..")")
  end

  for i = 1, slots.count() do
    local stack = slots()
    if stack['name'] ~= nil then
      if stack['enchantments'] ~= nil and stack.name ~= "minecraft:enchanted_book" then
        ts.transferItem(sourceSide, enchantedSide, stack.size, i)
      elseif itemMap[stack.name] ~= nil then
        ts.transferItem(sourceSide, itemMap[stack.name], stack.size, i)
      end
    end
  end
  sleep(5)
--  run = false
end