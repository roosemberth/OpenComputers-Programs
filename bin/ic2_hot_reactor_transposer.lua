-- Hot Reactor Control
-- (C) 2018 - Roosembert Palacios <roosembert.palacios@epfl.ch>
-- Released under CC BY-NC-SA License: https://creativecommons.org/licenses/
--
-- Removes coolant cells from IC2 nuclear reactors when their heat goes above a threshold.
-- Acts upon network messages "{}:RS:{} {}" with txn_id, side and redstone strength.
--
-- Requires a microcontroller with a transposer and a network and a redstone card.
--
-- luacheck: globals component computer checkArg
local msgPort = 36992

local itmsThreshold = {
  -- x < 0 => export on damage value < x
  -- x > 0 => export on damage value > x
  ["ic2:hex_heat_storage"] = 50000,
}

local srcInvNames = {"ic2:te"}

local cmp, cp, mf = component, computer, math.floor
local ts = cmp.proxy(cmp.list("transposer")())
local nw = cmp.proxy(cmp.list("modem")())
local rs = cmp.proxy(cmp.list("redstone")())

local pcs = {
  [ts] = "Missing transposer",
  [nw] = "Missing network card",
  [msgPort and msgPort > 0] = "Invalid broadcast port",
  [nw.open(msgPort)] = "Couldn't open port " .. msgPort
}

for pc, msg in pairs(pcs) do
  if not pc then error(msg) end
end

local function waitEvent(timeout, topics)
  local deadline = cp.uptime() + (timeout > 0 and timeout or 0)
  repeat
    local res = table.pack(cp.pullSignal(deadline - cp.uptime()))
    if not res then return end
    for a, b in pairs(topics) do if res[1] == a or res[1] == b then
      return res[1], table.pack(table.unpack(res, 2))
    end end
  until cp.uptime() >= deadline
end

local function shout(msg)
  if not nw.broadcast(msgPort, msg) then
    error ("Could not send message " .. msg)
  end
end

local function broadcastHeartbeat()
  shout(cp.address()..":"..mf(100*(cp.energy()-1)/cp.maxEnergy())..","..
        mf(100*(cp.freeMemory()-1)/cp.totalMemory())..","..mf(cp.uptime()/60))
end

local function getInvSidesAndStacks(srcInvs, invert)
  local function isSrcInv(name) if name == nil then return end
    for _, v in pairs(srcInvs) do if name == v then return true end end
    return false
  end
  local function it(s, _)
    for i = s.i, 5 do if (isSrcInv(ts.getInventoryName(i))) == (not invert) then
      s.i = i+1
      return i, ts.getAllStacks(i)
    end end
  end
  return it, {['i']=0}
end

local function shouldExport(stack, thres) local dmg
  if not thres then return end
  if stack['customDamage'] then
    dmg = stack.customDamage
  elseif stack['damage'] then
    dmg = stack.damage
  else
    return true
  end
  return (thres < 0 and dmg < -thres) or (thres > 0 and dmg > thres)
end

local function moveStackOut(fromSide, idx, size) local toSide
  for side, _ in getInvSidesAndStacks(srcInvNames, true) do toSide = side end
  if toSide then
    ts.transferItem(fromSide, toSide, size, idx)
  else
    error("Could not find exhaust inventory")
  end
end

local function export(fromSide, stacks)
  for i = 1, stacks.count() do local stack = stacks()
    if stack['name'] and shouldExport(stack, itmsThreshold[stack.name]) then
      moveStackOut(fromSide, i, stack.size)
    end
  end
end

local function rsMsgHandler(snd, port, sid, args)
  for side, strength in string.gmatch(args, "(%g+) (%g+)") do
    rs.setOutput(tonumber(side), tonumber(strength))
    nw.send(snd, port, sid .. ":OK")
  end
end

local msgHandlers = {
  ['modem_message'] = (function(_, snd, port, _, payload)
    for sid, op, arg in string.gmatch(tostring(payload), "(%d+):(%g+):(.+)$") do
      for k, v in pairs({['RS'] = rsMsgHandler}) do if k == op then
        local ok, err = pcall(v, snd, port, sid, arg)
        if not ok then nw.send(snd, port, sid .. ":ERR:" .. err) end
      end end
    end
  end)
}

while true do
  broadcastHeartbeat()
  local reactorFound = false
  for side, stacks in getInvSidesAndStacks(srcInvNames) do
    export(side, stacks)
    reactorFound = true
  end
  if not reactorFound then error("Reactor not found") end
  local topic, msg = waitEvent(1, msgHandlers)
  if topic then
    local ok, err = pcall(msgHandlers[topic], table.unpack(msg))
    if not ok then shout(":ERR:" .. msg .. "->" .. err) end
  end
end
