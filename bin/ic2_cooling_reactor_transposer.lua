-- Coolant Reactor Control
-- (C) 2018 - Roosembert Palacios <roosembert.palacios@epfl.ch>
-- Released under CC BY-NC-SA License: https://creativecommons.org/licenses/
--
-- This program moves coolant cells out of cooling nuclear reactors when their
-- internal heat goes bellow a threshold.
--
-- Requires a microcontroller with a transposer and a network card.
-- Will move items from any inventory whose name is in `srcInvNames` away. Magic!
--
-- luacheck: globals component computer checkArg
local msgPort = 36992

local itmsThreshold = {
  -- x < 0 => export on damage value < x
  -- x > 0 => export on damage value > x
  ["ic2:hex_heat_storage"] = -1,
}

local srcInvNames = {"ic2:te"}

local ts = component.proxy(component.list("transposer")())
local nw = component.proxy(component.list("modem")())
local cp = computer
local addr = cp.address()
local mf = math.floor

local preconditions = {
  [ts] = "This program requires a transposer",
  [nw] = "This program requires a network card",
  [addr] = "I couldn't read my own address",
  [msgPort and msgPort > 0] = "Invalid broadcast port"
}

for precondition, msg in pairs(preconditions) do
  if not precondition then error(msg) end
end

local function sleep(timeout)
  checkArg(1, timeout, "number", "nil")
  local deadline = cp.uptime() + (timeout or 0)
  repeat
    cp.pullSignal(deadline - cp.uptime())
  until cp.uptime() >= deadline
end

local function shout(msg)
  if not nw.broadcast(msgPort, msg) then
    error ("Could not send message " .. msg)
  end
end

local function broadcastHeartbeat()
  shout(addr..":"..mf(100*(cp.energy()-1)/cp.maxEnergy())..","..
        mf(100*(cp.freeMemory()-1)/cp.totalMemory())..","..mf(cp.uptime()/60))
end

local function getInvSidesAndStacks(srcInvs, invert)
  local function isSrcInv(name) if name == nil then return nil end
    for _, v in pairs(srcInvs) do if name == v then return true end end return false
  end
  local function it(s, _)
    for i = s.i, 5 do if (isSrcInv(ts.getInventoryName(i))) == (not invert) then
      s.i = i+1
      return i, ts.getAllStacks(i)
    end end
    return nil
  end
  return it, {['i']=0}
end

local function shouldExport(stack, thres) local dmg
  if stack['customDamage'] then
    dmg = stack.customDamage
  elseif stack['damage'] then
    dmg = stack.damage
  else
    error("Damage value for item " .. stack.name .. " couldn't be detected")
  end
  return (thres < 0 and dmg < -thres) or (thres > 0 and dmg > thres)
end

local function export(fromSide, stacks) local toSide
  for i = 1, stacks.count() do
    local stack = stacks()
    if stack['name'] and itmsThreshold[stack.name] then
      if shouldExport(stack, itmsThreshold[stack.name]) then
        for side, _ in getInvSidesAndStacks(srcInvNames, true) do toSide = side end
        if toSide then
          ts.transferItem(fromSide, toSide, stack.size, i)
        else
          error("Could not find exhaust inventory")
        end
      end
    end
  end
end

while true do
  broadcastHeartbeat()
  local reactorFound = false
  for side, stacks in getInvSidesAndStacks(srcInvNames) do
    export(side, stacks)
    reactorFound = true
  end
  if not reactorFound then error("Reactor not found") end
  sleep(1)
end
