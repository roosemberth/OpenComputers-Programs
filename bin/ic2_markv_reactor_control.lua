-- Industrialcraft 2 reactor control
-- (C) 2018 - Roosembert Palacios <roosembert.palacios@epfl.ch>
-- Released under CC BY-NC-SA License: https://creativecommons.org/licenses/
--
-- This program performs the following actions:
-- - Drives the reactor, keeps the heat bellow a threshhold
-- - Monitors coolant controllers and reports possible errors.
--
-- Requires
-- - A computer with a network card and an adapter connected to each reactor.
-- - A single “Hot Reactor Control” microcontroller for each reactor.
-- - A bunch of “Coolant Reactor Control” microcontrollers on cooling reactors.
--
-- Connected reactors may not be externally driven! this would interfere with
-- the autodiscovery protocol potentially causing meltdown!
--
-- luacheck: globals checkArg
local msgPort = 36992
local remoteWatchdogTimeout = 10
local networkTimeout = 10

local component = require("component")
local computer = require("computer")
local event = require("event")
local os = require("os")
local thread = require("thread")

local nw = component.proxy(component.list("modem")())
local rt = component.proxy(component.list("reactor")())

local preconditions = {
  [nw] = "This program requires a network card",
  [rt] = "I couldn't find the reactor",
  [msgPort and msgPort > 0] = "Invalid broadcast port"
}

for precondition, msg in pairs(preconditions) do
  if not precondition then error(msg) end
end

-- Domain-independent functions
local Session = {
  __call = function(ss, ...) local cb, res
    if ss.cb then
      cb, ss.cb = ss.cb, nil
      res = cb(ss, ...)
      if not ss.cb then ss:die() end -- Give cb the oportunity to `become`
      return res
    else error("Session closed ".. ss.id) end
  end,
  become = function(ss, cb) checkArg(1, cb, "function") ss.cb = cb end,
  die = function(ss) ss.cb = nil for _, dcb in pairs(ss.dcbs) do pcall(dcb, ss) end end,
  registerDeathCb = function(ss, dcb) ss.dcbs[#ss.dcbs+1] = dcb return ss end,
}
setmetatable(Session , {
  __call = function(_, sid, cb)
    checkArg(1, sid, "number") checkArg(2, cb, "function")
    return setmetatable({id = sid, cb = cb, dcbs = {}}, Session)
  end})
Session.__index = Session

local SessionManager = {
  __call = function(sm, sid) checkArg(1, sid, "number", "string", "nil")
    if sid then return sm.s[sid] else return nil end end,
  allSessions = function(sm) return sm.s end,
  open = function(sm, id, cb) if not cb then return end
    checkArg(1, id, "number", "nil") checkArg(2, cb, "function")
    if id and sm.s[id] then error("Session already exists") end
    id = (id or math.floor(math.random() * 255))
    if sm.s[id] == nil then
      sm.s[id], sm.n = Session(id, cb):registerDeathCb(sm:_cdcb(id)), sm.n + 1
      return id
    else return sm:open(nil, cb) end -- collision, retry
  end,
  _cdcb = function(sm, id)
    return function(_)
      if sm.s[id] then sm.s[id], sm.n = nil, sm.n-1 end
    end
  end,
}
SessionManager._index = SessionManager

-- Domain-dependent generic functions
local lastPingReceived = {}
local networkDevices = {}

local newSessionHandler = {
  ["PING"] = function(ss, snd, port)
    nw.send(snd, port, ss.id .. ":PONG")
  end,
  ["HB"] = function(ss, snd, _, arg)
    lastPingReceived[ss.id] = computer.uptime()
    for kind in string.gmatch(tostring(arg), "(%d+) (%d+)$") do for k, action in pairs({
        ['HRC'] = function() networkDevices[snd] = kind end, -- register
    }) do if kind == k then action() end end end
  end, -- Automatic registration :D
}

local msgHandlers = {
  ['modem_message'] = (function(_, snd, port, _, payload)
    for sid, op, arg in string.gmatch(tostring(payload), "(%d+):(%g+):(.+)$") do
      if not SessionManager(sid) then
        sid = SessionManager.open(sid, newSessionHandler[op])
      end
      SessionManager(sid)(snd, port, arg)
    end
  end)
}
for ev, cb in pairs(msgHandlers) do event.listen(ev, cb) end

local persistentWarns = {} -- id:msg
local SystemConsole = {}
setmetatable(SystemConsole, {__call = function(sc, msg) sc[#sc+1]=msg end})
-- Business logic
local rsAndCtrl = {}
local hrcAndSide = {}
local hrcDiscoveryRunning = {}

local function fosterReactorControllers()
  local agents = {}
  for addr, kind in pairs(networkDevices) do
    if kind == "HRC" and not hrcAndSide[addr] and not hrcDiscoveryRunning[addr] then
      local nextSideFn = nil
      for side = 0, 6 do
        nextSideFn = function(ss1)        -- TODO: Check for OK
          local associatedReactor
          for reactorAddr in component.list("reactor") do if not rsAndCtrl[reactorAddr] then
            if component.proxy(reactorAddr).producesEnergy() then
              associatedReactor = reactorAddr
              break
            end
          end end
          if not associatedReactor then
            ss1:become(function(ss2)      -- TODO: Check for OK
              nw.send(addr, msgPort, ss2.id..":RS:"..side.." "..0)
              ss2:become(nextSideFn)
            end)
            nw.send(addr, msgPort, ss1.id..":RS:"..side.." "..15)
          else
            rsAndCtrl[associatedReactor], hrcAndSide[addr] = addr, side
            ss1:become(function() end)    -- TODO: Check for OK
            nw.send(addr, msgPort, ss1.id..":RS:"..side.." "..0)
          end
        end
      end
      agents[addr] = nextSideFn
    end
  end
  local lastAgentDeathCb = function(ss)
    hrcDiscoveryRunning[ss.id] = nil
    assert(next(hrcDiscoveryRunning) == nil)
  end
  local mkAgentDeathCb = function(addr, cb, nextDeathCb) return function(ss)
    hrcDiscoveryRunning[addr] = computer.uptime() -- register next agent start time
    hrcDiscoveryRunning[ss.id] = nil
    SessionManager.open(addr, cb):registerDeathCb(nextDeathCb)
  end end
  for addr, cb in pairs(agents) do
    lastAgentDeathCb = mkAgentDeathCb(addr, cb, lastAgentDeathCb)
  end
  lastAgentDeathCb({id=0}) -- Ugly, but effective
end

local allThreads = {
  ['UI'] = thread.create(function() end),
  ['Reactor controller discovery'] = thread.create(function()
    while true do
      local ct = computer.uptime()
      local addr, timestamp = next(hrcDiscoveryRunning)
      if timestamp then
        if ct-timestamp > 6*networkTimeout then
          SessionManager(addr):die() -- TODO: Shutdown sides!
          SystemConsole("Killed agent with address"..addr)
        end
      else
        fosterReactorControllers()
      end
      os.sleep(1)
    end
  end),
  ['Remote watchdog'] = thread.create(function()
    while true do
      local now = computer.uptime()
      for host, v in pairs(lastPingReceived) do
        if now-v > remoteWatchdogTimeout then
          persistentWarns["timeout:"..host] = "I haven't heard from "..host.." in "..now-v.. "s"
        else
          persistentWarns["timeout:"..host] = nil
        end
      end
      os.sleep(1)
    end
  end),
  ['Reactors temp controll'] = thread.create(function() end),
}

while true do
  for thName, th in pairs(allThreads) do
    local thStatus = th:status()
    if thStatus == "dead" then
      error(thName .. "thread Died !")
    end
  end
  os.sleep(1)
end
